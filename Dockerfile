# Use an official Python runtime as a parent image
FROM python:3.6-slim

WORKDIR /app

COPY . /app

# Make port 80 available to the world outside this container
EXPOSE 8000

# Run app.py when the container launches
CMD python3 -m http.server 8000 --bind 0.0.0.0
